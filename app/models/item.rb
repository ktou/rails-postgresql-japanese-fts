require "bigram_tokenizer"

class Item < ApplicationRecord
  class << self
    def fts(query)
      return where if query.blank?
      tokenizer = BigramTokenizer.new(query)
      template, values = tokenizer.build_tsquery
      where("name_tsvector @@ (#{template})", *values)
        .or(where("to_tsvector('english', meaning) @@ " +
                  "to_tsquery('english', ?)",
                  query))
        .or(where("readings_tsvector @@ (#{template})", *values))
    end
  end

  before_save :update_name_tsvector
  before_save :update_readings_tsvector

  private
  def update_name_tsvector
    self.name_tsvector = build_tsvector(name)
  end

  def update_readings_tsvector
    self.readings_tsvector = build_tsvector(readings)
  end

  def build_tsvector(input)
    return nil if input.blank?
    tokenizer = BigramTokenizer.new(input)
    tokenizer.build_tsvector
  end
end
