# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_24_194730) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "items", force: :cascade do |t|
    t.text "name"
    t.tsvector "name_tsvector"
    t.text "meaning"
    t.text "readings", array: true
    t.tsvector "readings_tsvector"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index "to_tsvector('english'::regconfig, meaning)", name: "index_items_on_to_tsvector_english_meaning", using: :gin
    t.index ["name_tsvector"], name: "index_items_on_name_tsvector", using: :gin
    t.index ["readings_tsvector"], name: "index_items_on_readings_tsvector", using: :gin
  end

  create_table "tokens", force: :cascade do |t|
    t.text "name", null: false
    t.index ["name"], name: "index_tokens_on_name_text_pattern_ops", unique: true, opclass: :text_pattern_ops
  end

  create_table "x_items", id: false, force: :cascade do |t|
    t.text "name"
    t.tsvector "name_tsvector"
    t.text "meaning"
    t.text "readings", array: true
    t.tsvector "readings_tsvector"
    t.index "to_tsvector('english'::regconfig, meaning)", name: "items_meaning_index", using: :gin
    t.index ["name_tsvector"], name: "items_name_index", using: :gin
    t.index ["readings_tsvector"], name: "items_readings_index", using: :gin
  end

end
